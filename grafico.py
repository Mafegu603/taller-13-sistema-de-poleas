

class Grafico():
    def PrimerEscenario():
        import pygame 
        pygame.init
        Ancho= 600
        Alto = 600


        screen= pygame.display.set_mode((Ancho,Alto))


        running=True
        while running:

            for event in pygame.event.get():

                if event.type==pygame.QUIT:

                    running=False

            screen.fill((255, 255, 255))  

            pygame.draw.rect(screen,(240,128,114), [310, 150, 50, 50], 0)
            pygame.draw.circle(screen,(240,128,114), ((160,160)), 30)
            pygame.draw.polygon(screen,(0,0,0), [[300, 300],[20, 300],[300, 100]], 0)
            pygame.draw.circle(screen,(0,128,128), ((300,100)), 15)
            pygame.draw.aaline(screen, (0,0,0), [450, 300], [300, 100], 2)
            pygame.draw.aaline(screen, (0,0,0), [-10, 200], [300, 100], 2)
            

            pygame.display.update()
        pygame.quit()
        
Grafico.PrimerEscenario()